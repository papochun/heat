# Heat Calculator

This program was written to calculate the accumulated damage dealt from the **Heat Inherit** game mechanic
This should be paired with my [Damage Calculator Tool](https://gitlab.com/papochun/damage)


## Installation

Head over to: [The Go Programming Language](https://go.dev).  
Select **Download**, and select whatever platform you intend to install on.  
Once installed, you can run the command below in your terminal.  

```
go install gitlab.com/papochun/heat@latest
```


## Contact

You can contact me via this email:  
<papochun.unnamed123@slmail.me>
