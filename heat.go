package main

import (
	"fmt"
	"strconv"
	"log"
)

func getValue(prompt string, division float64, offset float64) (float64) {
	fmt.Printf(prompt)
	var buffer string
	fmt.Scanln(&buffer)

	buffer_float, err := strconv.ParseFloat(buffer, 64)
	if err != nil {
		log.Fatal(err)
	}
	return buffer_float / division + offset
}


func loopHeat(dps float64, duration float64) (float64){
	var total_dot float64
        for current_second := float64(0); current_second <= duration; current_second++ {
		total_dot += current_second * dps
        }
	total_dot *= float64(0.5)
	return total_dot
}

func main() {
	/* define values needed */
	fmt.Println("If you are only using 1 weapon, then re-use your weapon stats for the 'Primer' stats")
	fmt.Println("Don't use a Heat mod on 'Weapon' if using a 'Primer'")
	fmt.Println("Only enter faction damage to 'Primer' if your dps value already includes faction damage")
	fmt.Println("If no faction damage then enter '0' into both")

	dps := getValue("Damage Per Second: ", 1, 0)
	duration := getValue("Seconds of firing: ", 1, 0)

        p_elem := getValue("Primer: Heat: ", 100, 1)
        p_fact := getValue("Primer: Faction: ", 100, 1)

        fact := getValue("Weapon: Faction: ", 100, 1)
        sc   := getValue("Weapon: Status Chance: ", 100, 0)
        hsw  := getValue("Weapon: Heat Status Weight: ", 100, 0)

	/* calculating 'Primer' heat mod */
	dps = dps * p_elem
	/* calculating faction damage */
	dps = dps * p_fact * fact
	/* damage with heat proc */
	dps = dps * sc * hsw

	/* old functions */
	total_dot := loopHeat(dps, duration)
	fmt.Printf("Only Heat DoTs:       %0.f\n", total_dot)
	fmt.Printf("Only Direct Damage:   %0.f\n", dps * duration)
	fmt.Printf("DoTs + Direct Damage: %0.f\n", dps * duration + total_dot)
}
